let url = 'https://ajax.test-danit.com/api/swapi/films';

fetch(url)
    .then(response => response.json())
    .then(movies => {
        movies.forEach(film => {
            document.body.insertAdjacentHTML('beforeend', `
                <div class="episode${film.id}"
                <p><b>Номер эпизода:</b> ${film.episodeId}</p>
                <p><b>Название фильма:</b> ${film.name}</p>
                <p><b>Краткое содержание:</b> ${film.openingCrawl}</p>
                <ul class="characters-list${film.id}">
                </ul>
                <hr>
                </div>           
                `);
                
                
            const urls = film.characters;

            const requests = urls.map(url => fetch(url)
            .then(respnose => respnose.json()));

            Promise.all(requests)
            .then(data => data.forEach(person =>
                document.querySelector(`.characters-list${film.id}`).insertAdjacentHTML('beforeend', `<li>${person.name}</li>`)))
        }); 
    }); 
