class Employee {
    constructor (name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = `$${salary}USD`;
    }
    get name () {
        return this._name;
    }

    set name (newName) {
        this._name = newName;
    }

    get age(){
        return this._age;
    }
    set age(newAge){
        this._age = newAge;
    }
    get salary(){
        return this._salary
    }
    set salary(newSalary){
        this._salary = `$${newSalary} USD`
    }
}


class Programmer extends Employee {
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get programmerSalary() {
        return '$' + parseInt(this._salary.match(/\d+/) * 3) + ' USD';
    }
}



const programmer = new Programmer('Michail',15,500,'English, German, Ukrainian');
programmer.name = 'Michael';
programmer.age = 15;
programmer.salary = 500;
programmer.lang = 'UI/UX, German, English';
console.log(programmer);

const dariaProgrammer = new Programmer('Daria',27,1000,'English, JS');
dariaProgrammer.name = 'Dasha';
dariaProgrammer.age = 27;
dariaProgrammer.salary = 1000;
dariaProgrammer.lang = 'JS, English';
console.log(dariaProgrammer);

const vaniaProgrammer = new Programmer('Ivan',31,3000,'English, Italian, JS, C++, Python');
vaniaProgrammer.name = 'Ivan';
vaniaProgrammer.age = 31;
vaniaProgrammer.salary = 3000;
vaniaProgrammer.lang = 'English, Italian, JS, C++, Python';
console.log(vaniaProgrammer);