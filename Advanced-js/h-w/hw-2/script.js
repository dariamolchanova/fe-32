const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

const root = document.querySelector('#root'); 
const list = document.createElement('ul');
root.append(list);


let filteredBooks = books.map(item  => {
    const {author, name, price} = item;
    if (author && name && price) {
        return list.insertAdjacentHTML('beforeend', `<li>${author}, "${name}", ${price + ' ' + 'USD'}</li>`);
    }
}); 

function selectedBooks(bookItem) {
    const {author, name, price} = bookItem;
    if (!author) {
        throw new Error(`Добавьте название к книге ${name}, которая стоит ${price} USD`)
    } else if (!name) {
        throw new Error(`Добавьте название книги автора ${author}, которая стоит ${price}`)
    } else if (!price) {
        throw new Error(`Добавьте цену к книге ${author}  "${name}""`)
    }
}

for (let i = 0; i < books.length; i++) {
    try {
        selectedBooks(books[i])
        
    } catch (el) {
        console.error(el.message);
    }
}


