const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

gulp.task('compile-sass', function() {
    return gulp.src('./src/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));

});