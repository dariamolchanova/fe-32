import Product from "../components/Product";
import Header from "../components/Header/header";
import CartItem from "../components/CartItem/CartItem";

const Cart = (props) => {
    let totalPrice = 0;


    if(!props.products.length) {
        return <p>Loading</p>
    } else if (!props.cart.length) {
        return <>
        <Header />
        <p className='empty-folder'> No Products in Cart</p> 
        </>
    }
    const cartList = props.cart.map(cart => {
        const product = props.products.find(product => {
            return product.id === cart.id
        })



        if (!product) return null

        totalPrice += cart.quantity * product.price;


        return <li className='product product--cart' key={cart.id}>     
            <CartItem 
            title={product.title} 
            price={product.price} 
            image={product.image} 
            addToCart={props.addToCart} 
            id={product.id} 
            quantity={cart.quantity}
            decreaseQuantity={props.decreaseQuantity} 
            deleteFromCart={props.deleteFromCart}/>      
        </li> 
        
    }) 

    return <>
        <Header />
        <main className="cart-content">
            <ul className='product-list-container product-list-container--cart'>
                {cartList} 
            </ul>
            <div className="cart-total-container">
                <div className="cart-total">
                    <p className="cart-total-text">Total: {totalPrice} UAH</p>
                </div>
            </div>
        </main>
</>

}

export default Cart;