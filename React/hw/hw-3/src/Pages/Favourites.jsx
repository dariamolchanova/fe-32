import Product from "../components/Product";
import Header from "../components/Header/header";


const Favourites = (props) => {
    if(!props.products.length) {
        return <p>Loading</p>
    } else if (!props.favourites.length) {
        return <>
        <Header />
        <p className="empty-folder">No Favourites</p> 
        </>
    }
   
    const favouritesList = props.favourites.map(fav => {
        const product = props.products.find(product => {
            return product.id === fav.id
        }) 
        if (!product) return null
        
        return <li className='product' key={fav.id}>     
            <Product 
            title={product.title} 
            price={product.price} 
            image={product.image} 
            addToCart={props.addToCart} 
            toggleFav={props.toggleFavourite} 
            id={product.id} 
            isFavourite={true}  
            decreaseQuantity={props.decreaseQuantity} 
            deleteFromCart={props.deleteFromCart}/>      
        </li> 
    })

    return <>
        <Header />
        <ul className='product-list-container'>
            {favouritesList} 
        </ul>
</>

}

export default Favourites;