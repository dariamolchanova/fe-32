import Header from '../components/Header/header';
import Product from '../components/Product';


const Home = (props) => {

const productList = 
  props.products.map(product => {
  const isFavourite = props.favourites.some(fav => fav.id.toString() === product.id.toString() ) 

  return (
  <li className='product' key={product.id}>
    <Product 
    image={product.image} 
    title={product.title} 
    price={product.price} 
    id={product.id}
    addToCart={props.addToCart}
    toggleFav={props.toggleFavourite}
    isFavourite={isFavourite}
    decreaseQuantity={props.decreaseQuantity} 
    deleteFromCart={props.deleteFromCart}/>
  </li>)
})




return <>
    <Header/>
    <ul className='product-list-container'>
    {
    productList
    }
    </ul>
</>

}

export default Home;