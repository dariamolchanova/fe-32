const API_SERVER = "/api";

const PRODUCTS_PATH = `${API_SERVER}/products`;

const FAVOURITES_PATH = `${API_SERVER}/favourites`;

const CART_PATH = `${API_SERVER}/cart`;

const HEADERS = {
  "Content-Type": "application/json",
};

export const getProducts = async () => {
  const response = await fetch(PRODUCTS_PATH, {
    method: "GET",
    headers: HEADERS,
  });
  return await response.json();
};

export const getFavourites = async () => {
  const response = await fetch(FAVOURITES_PATH, {
    method: "GET",
    headers: HEADERS,
  });
  return await response.json();
};

export const deleteFromFavourites = async (id) => {
  await fetch(`${FAVOURITES_PATH}/${id}`, {
    method: "DELETE",
    headers: HEADERS,
  });
};

export const addToFavourites = async (id) => {
  const response = await fetch(FAVOURITES_PATH, {
    method: "POST",
    headers: HEADERS,
    body: JSON.stringify({
      id,
    }),
  });
  return await response.json();
};

export const addToCart = async (id, quantity) => {
  const response = await fetch(CART_PATH, {
    method: "POST",
    headers: HEADERS,
    body: JSON.stringify({
      id,
      quantity,
    }),
  });
  return await response.json();
};

export const getCart = async () => {
  const response = await fetch(CART_PATH, {
    method: "GET",
    headers: HEADERS,
  });
  return await response.json();
};

export const changeQuantity = async (id, quantity) => {
  const response = await fetch(`${CART_PATH}/${id}`, {
    method: "PUT",
    headers: HEADERS,
    body: JSON.stringify({
      quantity,
    }),
  });
  return await response.json();
};

export const deleteFromCart = async (id) => {
  await fetch(`${CART_PATH}/${id}`, {
    method: "DELETE",
    headers: HEADERS,
  });
};
