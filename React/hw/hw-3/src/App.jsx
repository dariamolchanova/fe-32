import './App.scss';
import { useState, useEffect } from 'react';
import Product from './components/Product';
import { addToFavourites, deleteFromFavourites, getCart, getFavourites, getProducts, changeQuantity, addToCart as addToCartOnServer, deleteFromCart as deleteFromCartOnServer } from './API/requests';
import AppRoutes from './Routes/routes';

/**
 * TODO:
 * requests for cart ✅
 * render cart items
 * add to cart, delete from cart, decrease cart quantity ✅
 */

const App = (props) => {

  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [favourites, setFavourites] = useState([]);

useEffect(() => {
  getFavourites()
  .then(favourites => setFavourites(
    favourites
  ))
}, [])

useEffect(() => {
  getCart()
  .then(cart => setCart(
    cart
  ))
}, [])

  useEffect(() => { 
    getProducts()
    .then(products => setProducts(
      products
    ))   
  }, []
)
  
  const addToCart = (id) => {
    const cartItem = cart.find(product => product.id === id);
    if (cartItem) {
      /* создаем копию обьекта продукта в корзине (cartItemCopy) и увеличиваем его кол-во, 
      причина: 1) принцип immutable 2) для явного оновления стейта.
      Потом делаем запрос на сервер для обновления информации там. 
      При успешном обновлении на сервере - мы получаем обьект с обновленным количеством. 
      После мы используем конструкцию then(), чтобы быть уверенным что обновился стейт только после того как успешно обновился сервер (db.json). 
      внутри then мы делаем копию, так как нам нужно ее изменить. Будем ее менять методом массива splice, который изменяет исходный массив. Причина та же что и для cartItemCopy. Для работы метода splice нужен index элемента с которого мы хотим начать замену, потому мы его находим методом findIndex. и в итоге мы вырезаем старый обьект, заменяем его на новый и обновленную корзину сетим в стейт 
      */
      
      changeQuantity(id, Number(cartItem.quantity) + 1)
      .then((newCartItem) => {
        const cartCopy = [
          ...cart
        ]
        const index = cartCopy.findIndex(item => item.id === id);
        cartCopy.splice(index, 1, newCartItem)
        setCart(cartCopy)
      })
    } else {
      addToCartOnServer(id, 1)
      .then((newCartItem) => {
        const cartCopy = [
          ...cart, 
          newCartItem,
        ]
        setCart(cartCopy);
      })
    }
}

const deleteFromCart = (id) => {
  deleteFromCartOnServer(id)
  .then(() => {
    const newCart = cart.filter(product => product.id !== id)
    setCart(newCart)
  })
} 

const decreaseQuantity = (id) => {
  const cartItem = cart.find(product => product.id === id)
  if (cartItem.quantity > 1) {
    changeQuantity(id, Number(cartItem.quantity) - 1)
    .then((newCartItem) => {
      const cartCopy = [
        ...cart
      ]
      const index = cartCopy.findIndex(item => item.id === id);
      cartCopy.splice(index, 1, newCartItem)
      setCart(cartCopy)
    })
  } else {
    
    deleteFromCart(id)
  }
  
 }

const toggleFavourite = (id) => {
  const isInFavourites = favourites.some(fav => fav.id === id)
  let newFav
  if (isInFavourites) {
      newFav = favourites.filter(fav => {
      return fav.id !== id
    }) 
    deleteFromFavourites(id);
    
  } else {
    newFav = [
      ...favourites, {id}
    ]
    addToFavourites(id);
  }

  setFavourites(newFav)
}


return <AppRoutes cart={cart} favourites={favourites} products={products} addToCart={addToCart} toggleFavourite={toggleFavourite} decreaseQuantity={decreaseQuantity} deleteFromCart={deleteFromCart} />

}



export default App;
