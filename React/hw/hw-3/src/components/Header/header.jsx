import { Link, NavLink } from "react-router-dom";
import Logo from "../../assets/logo.png";
import styles from "./header.module.scss";
import cx from 'classnames'

const Header = () => {
    return (
        <header className={styles.header}>
            <div className={styles.inner}>
                <Link className={styles.logo} to = '/'>
                <img src={Logo} alt="Logo" />
                </Link>
                <ul className={styles.navbar}>
                <li className={styles.navbarItems}>
                        <NavLink className={({ isActive }) =>
                            isActive ? cx(styles.active, styles.navbarLink) : styles.navbarLink
                        } to = '/'>
                        Home
                        </NavLink>
                    </li>
                    <li className={styles.navbarItems}>
                        <NavLink className={({ isActive }) =>
                            isActive ? cx(styles.active, styles.navbarLink) : styles.navbarLink
                        } to = '/favourites'>
                        Favourites
                        </NavLink>
                    </li>
                    <li className={styles.navbarItems}>
                        <NavLink className={({ isActive }) =>
                            isActive ? cx(styles.active, styles.navbarLink) : styles.navbarLink
                        } to = '/cart'>
                        Cart
                        </NavLink>
                    </li>
                </ul>
            </div>
        </header>
    )
}

export default Header;