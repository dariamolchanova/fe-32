import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Buttons.scss'

const Button = (props) => {
    const { backgroundColor, text, onClick, className, ...rest } = props;
    const buttonStyles = {
        backgroundColor: backgroundColor,
    }

    return (
        <button 
            {...rest}
            onClick={onClick} 
            className={className}
            style={buttonStyles}>
            {text}
        </button>
        
    )

}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string,
}

Button.defaultProps = {
    backgroundColor: 'blue',
}



export default Button