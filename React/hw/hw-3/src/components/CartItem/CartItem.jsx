import styles from './CartItem.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import Modal from '../Modal/Modal';
import { useState } from 'react';
import { modalIDs, findModalConfig } from '../Modal/modalConfig'
import Button from '../Button/Button';


const CartItem = (props) => {
const { title, price, image, id, quantity, addToCart, decreaseQuantity, deleteFromCart } = props;

const [modalOpen, setModalOpen] = useState(false);

const modalConfig = findModalConfig(modalIDs.deleteFromCart);

const handleModalClose = () => {
    setModalOpen(false)
}

const handleModalOpen = () => {
    setModalOpen(true)
}


const actions = <div className='modalBtns'>
        <Button backgroundColor='rgb(158, 183, 206)' text='Yes' onClick={() => deleteFromCart(id)} className={styles.modalBtn}/>
        <Button backgroundColor='rgb(158, 183, 206)' text='No' onClick={handleModalClose} className={styles.modalBtn}/>
    </div>

    return (<>

        {modalOpen && <Modal closeButton={true} actions={actions} onClose={handleModalClose} {...modalConfig}/>}
        <div className={styles.cartItem} id={id}>     
        <figure className={styles.cartItemImg}>
            <img src={image} alt={title} />
        </figure>
        <div className={styles.cartItemTitle}>
            <p>{title}</p>
        </div>
        <div className={styles.cartItemCounter}>
            <div>
                <button className={styles.cartItemCounterButton} onClick={() => decreaseQuantity(id)} disabled={quantity === 1}>
                    <FontAwesomeIcon icon={faMinus}/>
                </button>
                <span className={styles.cartItemCounterNumber}>{quantity}</span>
                <button className={styles.cartItemCounterButton} onClick={() => addToCart(id)}>
                    <FontAwesomeIcon icon={faPlus}/>
                </button>
            </div>
        </div>
        <div className={styles.cartItemPrice}>
            <span>
                {price * quantity} UAH
            </span>
        </div>
        <div className={styles.cartItemTrash}>
            <button className={styles.cartItemTrashIcon} onClick={handleModalOpen}>
                <FontAwesomeIcon icon={faTrash}/>
            </button>
            </div>
        </div>
        </>
    )
}

export default CartItem