import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from'./Modal.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const Modal = (props) => {
    const onModalClick = (e) => {
        if (e.target === e.currentTarget) {
            props.onClose()
        }
    }
    return (
        <div className={styles.modal} onClick={onModalClick}>
            <div className={styles.modalContent}>
                <div className={styles.modalHeader}>
                    <p>{props.header}</p>
                    {props.closeButton && <button onClick={props.onClose} className={styles.closeBtn}><FontAwesomeIcon icon={faTimes}/></button>}
                </div>

                <div className={styles.modalBody}>
                    <p>{props.text}</p>
                    
                </div>

                <div className={styles.modalFooter}>
                    {props.actions}
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.string.isRequired,
    actions: PropTypes.element,
    className: PropTypes.string,
    onClose: PropTypes.func.isRequired,

}
Modal.defaultProps = {
    closeButton: true,
    className: 'modal',
}




export default  Modal