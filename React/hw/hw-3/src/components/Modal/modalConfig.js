export const modalIDs = {
  addToCart: 1,
  deleteFromCart: 2,
};


export const modalContent = [
  {
    id: modalIDs.addToCart,
    header: "Do you want to add a product to cart?",
    text: "This item will be added to your cart",
  },

  {
    id: modalIDs.deleteFromCart,
    header: "Do you want to delete from cart?",
    text: "This item will be deleted from your cart",
  },
];

export const findModalConfig = (id) => {
  const config = modalContent.find(item => {
   return item.id.toString() === id.toString()
  }) 
  return config;
}


