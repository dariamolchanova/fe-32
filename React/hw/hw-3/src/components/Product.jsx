import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Product.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons';
import Modal from './Modal/Modal';
import { modalIDs, findModalConfig } from './Modal/modalConfig';
import Button from './Button/Button';


const Product = (props) => {

const [modalOpen, setModalOpen] = useState(false);



    const {image, title, price, id, addToCart, toggleFav, isFavourite} = props
        
    const modalConfig = findModalConfig(modalIDs.addToCart);

    const handleModalClose = () => {
        setModalOpen(false)
    }

    const handleModalOpen = () => {
        setModalOpen(true)
    }

    const handleAddToCartClick = () => {
        addToCart(id);
        handleModalClose();
    }

    const actions = <div className='modalBtns'>
        <Button backgroundColor='rgb(158, 183, 206)' text='Yes' onClick={handleAddToCartClick} className={styles.modalBtn}/>
        <Button backgroundColor='rgb(158, 183, 206)' text='No' onClick={handleModalClose} className={styles.modalBtn}/>
    </div>
    
    return (

        <>
            {
                modalOpen && <Modal closeButton={true} actions={actions} onClose={handleModalClose} {...modalConfig}/>
            }

            <div id={id} className={styles.productCard}>
                <figure className={styles.image}>
                    <img src={image} alt={title} />
                </figure>
                <div className={styles.productCardInfo}>
                    <p className={styles.productCardTitle}>{title}</p>
                    <p className={styles.productCardPrice}>{price}</p>
                </div>
                <button onClick={handleModalOpen} className={styles.productCardBtns}>Add to Cart</button>
                <button className={styles.productCardBtns} onClick={() => toggleFav(id)}><FontAwesomeIcon icon={isFavourite ? faHeartSolid : faHeartRegular}/></button>
            </div>
        </>
    )
}


Product.propTypes = {
    title: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    color: PropTypes.object,
    addToCart: PropTypes.func.isRequired,
    toggleFav: PropTypes.func.isRequired,
    isFavourite: PropTypes.bool.isRequired,
    decreaseQuantity: PropTypes.func.isRequired,
    deleteFromCart: PropTypes.func.isRequired,
}



export default Product;

