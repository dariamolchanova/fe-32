import * as React from "react";
import { Routes, Route, Link } from "react-router-dom";
import Home from "../Pages/Home";
import Favourites from "../Pages/Favourites";
import Cart from "../Pages/Cart";

const AppRoutes = (props) => {
    

    return (
          <Routes>
            <Route path="/" element={<Home {...props}/>} />
            <Route path="/favourites" element={<Favourites {...props}/>} />
            <Route path="/cart" element={<Cart {...props}/>} />
          </Routes>
    )
}

export default AppRoutes;