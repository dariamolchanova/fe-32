import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Buttons.scss'

class Button extends Component {
    static propTypes = {
        backgroundColor: PropTypes.string,
        text: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        className: PropTypes.string,
    }

    static defaultProps = {
        backgroundColor: 'red',
    }

    render() {
        const { backgroundColor, text, onClick, className, ...rest } = this.props
        const buttonStyles = {
            backgroundColor: backgroundColor,
        }
       
        return (
            <button 
                {...rest}
                onClick={onClick} 
                className={className}
                style={buttonStyles}>
                {text}
            </button>
            
        )
    }
}

export default Button