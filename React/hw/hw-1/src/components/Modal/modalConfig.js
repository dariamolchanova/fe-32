export const modalIDs = {
  deleteFile: 1,
  beliveInLove: 2,
};

export const modalContent = [
  {
    id: modalIDs.deleteFile,
    header: "Do you want to delete this file?",
    text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
  },

  {
    id: modalIDs.beliveInLove,
    header: "Do you?",
    text: "Do you belive in love after love?",
  },
];
