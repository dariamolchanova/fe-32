import Button from './components/Button/Button.jsx'
import { Component } from 'react';
import Modal from './components/Modal/Modal.jsx';
import { modalIDs, modalContent } from './components/Modal/modalConfig.js';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpenId: '',
    }
  }
  
  render() {
    
    const closeModal = () => {
      this.setState({
        modalOpenId: ''
      })
    }

    const modalBtns = {
      [modalIDs.deleteFile]: (
        <div className="btns-container">

          <Button 
            text ="OK" 
            backgroundColor ="rgb(112, 21, 5)"
            onClick = {() => {
              closeModal()
            }}
            className = "btn modal-btn"
          />

          <Button 
            text ="Cancel" 
            backgroundColor ="rgb(112, 21, 5)"
            onClick = {() => {
              closeModal()
            }}
            className = "btn modal-btn"
          />
        </div>
      ),

      [modalIDs.beliveInLove]: (
      <div className="btns-container">
        <Button 
          text ="Yes!" 
          backgroundColor ="rgb(112, 21, 5)"
          onClick = {() => {
            closeModal()
          }}
          className = "btn modal-btn"
        />
        
        <Button 
          text ="No!" 
          backgroundColor ="rgb(112, 21, 5)"
          onClick = {() => {
            closeModal()
          }}
          className = "btn modal-btn"
        />
      </div>
      )
    }
  
      
    
  const handleClick = (e) => {
    this.setState({
      modalOpenId: e.target.dataset.modalid
    })
  }

  const findModalConfig = (id) => {
    const config = modalContent.find(item => {
     return item.id.toString() === id.toString()
    }) 
    return config;
  }

    return (

      <div className="App">

        <Button 
          text ="Open first modal" 
          backgroundColor ="gold"
          data-modalID = {modalIDs.deleteFile}
          className = "btn"
          onClick = {handleClick}
        />
  
        <Button 
        text ="Open second modal"
        backgroundColor ="silver"
        data-modalID = {modalIDs.beliveInLove}
        className = "btn"
        onClick = {handleClick}
        />

      {this.state.modalOpenId && 
      <Modal 
        {...findModalConfig(this.state.modalOpenId)} 
        onClose={closeModal}
        actions={modalBtns[this.state.modalOpenId]}

      />}

      
        
      </div>
    );
  }
 
}

export default App;
