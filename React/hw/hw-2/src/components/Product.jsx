import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from './Product.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartRegular } from '@fortawesome/free-regular-svg-icons';
import Modal from './Modal/Modal';
import { modalIDs, findModalConfig } from './Modal/modalConfig';
import Button from './Button/Button';

class Product extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
        id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        color: PropTypes.object,
        addToCart: PropTypes.func.isRequired,
        toggleFav: PropTypes.func.isRequired,
        isFavourite: PropTypes.bool.isRequired,
    }
    state = {
        modalOpen: false,
    }

    render() {
        const {image, title, price, id, color, addToCart, toggleFav, isFavourite} = this.props
        
        const modalConfig = findModalConfig(modalIDs.addToCart);

        const handleModalClose = () => {
            this.setState({...this.state, modalOpen: false})
        }

        const handleModalOpen = () => {
            this.setState({...this.state, modalOpen: true})
        }

        const handleAddToCartClick = () => {
            addToCart(id);
            handleModalClose();
        }

        const actions = <div className='modalBtns'>
            <Button backgroundColor='rgb(158, 183, 206)' text='Yes' onClick={handleAddToCartClick} className={styles.addToCartBtn}/>
            <Button backgroundColor='rgb(158, 183, 206)' text='No' onClick={handleModalClose} className={styles.addToCartBtn}/>
        </div>
        
        return (

            <>
                {
                    this.state.modalOpen && <Modal closeButton={true} actions={actions} onClose={handleModalClose} {...modalConfig}/>
                }

                <div id={id} className={styles.productCard}>
                    <figure className={styles.image}>
                        <img src={image} alt={title} />
                    </figure>
                    <div className={styles.productCardInfo}>
                        <p className={styles.productCardTitle}>{title}</p>
                        <p className={styles.productCardPrice}>{price}</p>
                    </div>
                    <button onClick={handleModalOpen} className={styles.productCardBtns}>Add to Cart</button>
                    <button className={styles.productCardBtns} onClick={() => toggleFav(id)}><FontAwesomeIcon icon={isFavourite ? faHeartSolid : faHeartRegular}/></button>
                </div>
            </>
        )
    }
}

export default Product;