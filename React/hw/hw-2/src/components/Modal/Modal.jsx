import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styles from'./Modal.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'


class Modal extends Component {
    static propTypes = {
        header: PropTypes.string.isRequired,
        closeButton: PropTypes.bool,
        text: PropTypes.string.isRequired,
        actions: PropTypes.element,
        className: PropTypes.string,
        onClose: PropTypes.func.isRequired,

    }
    static defaultProps = {
        closeButton: true,
        className: 'modal',
    }

    render() {
        const onModalClick = (e) => {
            if (e.target === e.currentTarget) {
                this.props.onClose()
            }
        }
        return (
            <div className={styles.modal} onClick={onModalClick}>
                <div className={styles.modalContent}>
                    <div className={styles.modalHeader}>
                        <p>{this.props.header}</p>
                        {this.props.closeButton && <button onClick={this.props.onClose} className={styles.closeBtn}><FontAwesomeIcon icon={faTimes}/></button>}
                    </div>

                    <div className={styles.modalBody}>
                        <p>{this.props.text}</p>
                        
                    </div>

                    <div className={styles.modalFooter}>
                        {this.props.actions}
                    </div>
                </div>
            </div>
        )
    }
}


export default  Modal