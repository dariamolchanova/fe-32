import './App.scss';
import { Component } from 'react';
import Product from './components/Product';



class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      products: [],
      cart: [],
      favourites: [],
    }
  }
  
  render() {

    const addToCart = (id) => {
        const newCart = [
          ...this.state.cart, id
        ]
        this.setState({...this.state, cart: newCart})
        localStorage.setItem('cart', JSON.stringify(newCart))
    }

    const toggleFavourite = (id) => {
      const isInFavourites = this.state.favourites.some(fav => fav === id)
      let newFav
      if (isInFavourites) {
          newFav = this.state.favourites.filter(fav => {
          return fav !== id
        }) 
        
      } else {
        newFav = [
          ...this.state.favourites, id
        ]
      }

      this.setState({...this.state, favourites: newFav})
      localStorage.setItem('favourites', JSON.stringify(newFav))
    }

    const productList = 
    this.state.products.map(product => {
      const isFavourite = this.state.favourites.some(id => id.toString() === product.id.toString() ) 

      return (
      <li className='product' key={product.id}>
        <Product 
        image={product.image} 
        title={product.title} 
        price={product.price} 
        id={product.id}
        addToCart={addToCart}
        toggleFav={toggleFavourite}
        isFavourite={isFavourite}/>
      </li>)
    })

    console.log('Cart: ', this.state.cart);
    console.log('Favourites: ', this.state.favourites);



    return <>
    <header className='header'> Logo</header>
    <ul className='productListContainer'>
      {
      productList
      }
    </ul>
    </>
  }
 
  componentDidMount() {
    fetch('/goods.json')
    .then(response => response.json())
    .then(products => this.setState({
      ...this.state, products:products.allGoods
    }))
    .then(() => {
      let cart = localStorage.getItem('cart')
      let favourites = localStorage.getItem('favourites')

      cart = JSON.parse(cart);
      favourites = JSON.parse(favourites);

      if (Array.isArray(cart)) {
        this.setState({
          ...this.state, cart
        })
      }
      if (Array.isArray(favourites)) {
        this.setState({
          ...this.state, favourites
        })
      }
    })
  }
}

export default App;
